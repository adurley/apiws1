<?php
/**
* 
*/
include_once('../config/database.php');
class Automovil

{

	private $marca;
	private $placa;
	private $color;
	private $modelo;
	private $tipoauto;
	public $insert;
	public $getAll;
	public $getOne;
	public $update;
	public $delete;

	function newAutomovil($marca, $placa, $color, $modelo, $tipoauto)
	{
		$this->marca = $marca;
		$this->placa = $placa;
		$this->color = $color;
		$this->modelo = $modelo;
		$this->tipoauto = $tipoauto;
	}

	function insertAutomovil(){
		$db = new database;
		$con = $db->conect();
		if($con->connect_error){
			$this->insert=false;
		}else{
			$result = $con->query("INSERT INTO automovil (marca, placa, color, modelo, tipoauto) VALUES ('{$this->marca}','{$this->placa}','{$this->color}',{$this->modelo},'{$this->tipoauto}');");
			if($result=== true){
				$this->insert = true;
			}else{
				if($con->error === "Duplicate entry '{$this->placa}' for key 'placa'" ){
					$this->insert="Duplicate";
				}else{
					$this->insert=$con->error;
				}
			}
		}
		return $this->insert;
	}

	//Ver todos los autómoviles
	function getAutomovil(){
		$db = new database;
		$con = $db->conect();
		$result = $con->query("SELECT marca, placa, color, modelo, tipoauto FROM automovil;");
		if($result->num_rows === 0){
				echo json_encode(array("messaje" => "No automovil found."));
			}else{
				$products_arr=array();
    			$products_arr["records"]=array();
				while($fila=$result->fetch_assoc()){
				//$this->getAll[]=$fila;
					extract($fila);
					$url = "http://localhost/api/automovil/api.php?view&placa=".$placa;
					$product_item=array(
			            "marca" => $marca,
			            "placa" => $placa,
			            "url" => $url,
			            "color" => $color,
			            "modelo" => $modelo,
			            "tipoauto" => $tipoauto
			        );
	 
	        		array_push($products_arr["records"], $product_item);
    			}
 
    			echo json_encode($products_arr);
			}
	}

	//Ver un automovil
	function getAuto($placa){
		$db = new database;
		$con = $db->conect();
		$result = $con->query("SELECT marca, placa, color, modelo, tipoauto FROM automovil WHERE placa='{$placa}';");
		if($result->num_rows === 0){
				echo json_encode(array("messaje" => "No automovil found."));
			}else{
				$products_arr=array();
				while($fila=$result->fetch_assoc()){

					$product_arr = array(
			    		"marca" => $fila['marca'],
			            "placa" => $fila['placa'],
			            "color" => $fila['color'],
			            "modelo" => $fila['modelo'],
			            "tipoauto" => $fila['tipoauto']
			        );
				}
				
				print_r(json_encode($product_arr));
			}
	}

	//Eliminar un automovil
	function deleteAutomovil($placa){
		$db = new database;
		$con = $db->conect();
		$result = $con->query("DELETE FROM automovil WHERE placa = '{$placa}';");
		$this->delete=false;
		if($result === true){
			$this->delete = true;
		}else{
				$this->delete=$con->error;
			}
			return $this->delete;
		}
	

	//Actualizar un automovil
	function updateAutomovil($marca,$placa, $color, $modelo, $tipoauto){
		$db = new database;
		$con = $db->conect();
		$result = $con->query("UPDATE automovil SET marca='{$marca}', color='{$color}', modelo= {$modelo}, tipoauto='{$tipoauto}' WHERE placa = '{$placa}';");
		if($result=== true){
			$this->update = true;
		}else{
				$this->update=$con->error;
			}
		return $this->update;
	}

	
}
?>