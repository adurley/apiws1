<?php
/**
* 
*/
include_once('../config/database.php');
class Automovil

{

	private $marca;
	private $placa;
	private $color;
	private $modelo;
	private $tipoauto;
	public $insert;
	public $getAll;
	public $getOne;
	public $update;
	public $delete;

	function newAutomovil($marca, $placa, $color, $modelo, $tipoauto)
	{
		$this->marca = $marca;
		$this->placa = $placa;
		$this->color = $color;
		$this->modelo = $modelo;
		$this->tipoauto = $tipoauto;
	}

	function insertAutomovil(){
		$db = new database;
		$con = $db->conect();
		if(!$con){
			echo json_encode(array("messaje" => "Error connecting Database."));
		}else{
			$result = pg_query($con, "INSERT INTO automovil (marca, placa, color, modelo, tipoauto) VALUES ('{$this->marca}','{$this->placa}','{$this->color}',{$this->modelo},'{$this->tipoauto}');");
			if (stripos(pg_last_error($con), "duplicate key value") !== false){
				echo json_encode(array("messaje" => "Error Duplicate entry for Placa. Car {$this->placa} already exists."));

			}else{
				if(!$result){
					echo json_encode(array("messaje" => "Error creating the car."));
				}else{
					echo json_encode(array("messaje" => "Car {$this->placa} created correctly."));
				}
			}
			
		}
	}

	//Ver todos los autómoviles
	function getAutomovil(){
		$db = new database;
		$con = $db->conect();
		if(!$con){
			echo json_encode(array("messaje" => "Error connecting Database."));
		}else{
		$result = pg_query($con, "SELECT marca, placa, color, modelo, tipoauto FROM automovil;");
			if(!$result){
				echo json_encode(array("messaje" => "Error query."));
			}elseif (pg_num_rows($result) == 0){
				echo json_encode(array("messaje" => "Error query."));
			}else{
				$products_arr=array();
    			$products_arr["records"]=array();
				while($fila=pg_fetch_row($result)){
				//$this->getAll[]=$fila;
					$url = "http://".$_SERVER['SERVER_NAME']."/automovil/api.php?view&placa=".$fila[1];
					$product_item=array(
			            "marca" => $fila[0],
			            "placa" => $fila[1],
			            "url" => $url,
			            "color" => $fila[2],
			            "modelo" => $fila[3],
			            "tipoauto" => $fila[4]
			        );
	 
	        		array_push($products_arr["records"], $product_item);
    			}
 
    			echo json_encode($products_arr);
			}
		}
	}

	//Ver un automovil
	function getAuto($placa){
		$db = new database;
		$con = $db->conect();
		if(!$con){
			echo json_encode(array("messaje" => "Error connecting Database."));
		}else{
		$result = pg_query($con, "SELECT marca, placa, color, modelo, tipoauto FROM automovil WHERE placa='{$placa}';");
		if(!$result){
				echo json_encode(array("messaje" => "Error query."));
			}elseif (pg_num_rows($result) == 0){
				echo json_encode(array("messaje" => "Error query."));
			}else{
				$products_arr=array();
				while($fila=pg_fetch_row($result)){

					$product_arr = array(
			    		"marca" => $fila[0],
			            "placa" => $fila[1],
			            "color" => $fila[2],
			            "modelo" => $fila[3],
			            "tipoauto" => $fila[4]
			        );
				}
				
				print_r(json_encode($product_arr));
			}
		}
	}

	//Eliminar un automovil
	function deleteAutomovil($placa){
		$db = new database;
		$con = $db->conect();
		$result = $con->query("DELETE FROM automovil WHERE placa = '{$placa}';");
		$this->delete=false;
		if($result === true){
			$this->delete = true;
		}else{
				$this->delete=$con->error;
			}
			return $this->delete;
		}
	

	//Actualizar un automovil
	function updateAutomovil($marca,$placa, $color, $modelo, $tipoauto){
		$db = new database;
		$con = $db->conect();
		$result = $con->query("UPDATE automovil SET marca='{$marca}', color='{$color}', modelo= {$modelo}, tipoauto='{$tipoauto}' WHERE placa = '{$placa}';");
		if($result=== true){
			$this->update = true;
		}else{
				$this->update=$con->error;
			}
		return $this->update;
	}

	
}
?>