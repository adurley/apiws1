function onSignIn(googleUser) {
  var profile = googleUser.getBasicProfile();
  $("#nameapigoogle").text(profile.getName() + ". Try out this example's API!");
  $(".g-signin2").attr('onclick',"signOut();");
  var x = document.querySelector(".g-signin2");
  var y= x.querySelector("span span:nth-child(2)");
  y.remove();
  var m = x.querySelector("span");
  m.insertAdjacentHTML('beforeend',"<span>Sign Out</span>");
  $(".g-signin2").removeAttr('data-onsuccess');
  console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
  console.log('Name: ' + profile.getName());
  console.log('Image URL: ' + profile.getImageUrl());
  console.log('Email: ' + profile.getEmail()); // This is null if the 'email' scope is not present.
}

function signOut() {
    var auth2 = gapi.auth2.getAuthInstance();
    auth2.signOut().then(function () {
      alert('User signed out.');
      $(".g-signin2").removeAttr('onclick');
      $("#nameapigoogle").text("Try out this example's API!");
      $(".g-signin2").attr('data-onsuccess',"onSignIn");
    });
  }